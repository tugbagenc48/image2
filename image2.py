#!/home/z/anaconda3/bin/python
from PIL import Image
from scipy.signal import find_peaks
import os
import img2pdf
from shutil import copyfile

dir = []
fileList = []
rateList = []

os.mkdir("./pdf")

for i in sorted(os.listdir('.')):
    if i.endswith(".jpg"):
        dir.append(i)

for a in range (1, len(dir)):
    
    image0 = Image.open(dir[a]).convert('LA')
    image1 = Image.open(dir[a-1]).convert('LA')
    
    pairs0 = zip(image0.getdata(), image1.getdata())
    dif0 = sum(abs(c1-c2) for p1,p2 in pairs0 for c1,c2 in zip(p1,p2))
    
    ncomponents = image0.size[0] * image0.size[1] * 3
    
    rate = (dif0 / 255.0 * 100) / ncomponents
    
    print(dir[a], rate)
    fileList.append(dir[a])
    rateList.append(rate)
    
indices = find_peaks(rateList, threshold=0.05)[0]

copyfile("./00000.jpg", "./pdf/00000.jpg")
for i in indices:
    copyfile(fileList[i], "./pdf/" + fileList[i]) 

name = os.getcwd().split("/")[-1]
os.chdir("./pdf")

with open(name+".pdf", "wb") as f:
    f.write(img2pdf.convert([i for i in sorted(os.listdir("./")) if i.endswith(".jpg")]))
