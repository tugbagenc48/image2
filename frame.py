#!/home/z/anaconda3/bin/python
import cv2
import os
from shutil import copyfile

dir = []

for i in sorted(os.listdir('.')):
    if i.endswith(".mp4"):
        dir.append(i)

for vid in dir:
    name = vid.split(".")[0]
    if not os.path.isdir(name):
        os.mkdir(name)
        print(name)
        count = 0
        vid = r"./"+vid
        vidcap = cv2.VideoCapture(vid)
        def getFrame(sec):
            vidcap.set(cv2.CAP_PROP_POS_MSEC,sec*1000)
            hasFrames,image = vidcap.read()
            if hasFrames:
                cv2.imwrite(name+"/{0:05d}".format(count)+".jpg", image)
            return hasFrames
        sec = 0
        frameRate = 2
    
        success = getFrame(sec)
        while success:
            count = count + 1
            sec = sec + frameRate
            sec = round(sec, 2)
            success = getFrame(sec)
        copyfile("./image2.py", "./{}/image2.py".format(name))
